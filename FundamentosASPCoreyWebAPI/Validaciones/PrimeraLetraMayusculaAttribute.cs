using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FundamentosASPCoreYWebAPI.Validaciones {

public class PrimeraLetraMayusculaAttribute: ValidationAttribute {
    protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
    {
        if (value == null || string.IsNullOrEmpty(value.ToString()))
        {
            // con esto lo que se busca es que unicamente la validacion se encargue de vakidadr que la primera letra es mayuscula y lo segundo es que no se solape con la validacion Required
            return ValidationResult.Success;
        }

        var primeraLetra = value.ToString()[0].ToString();

        if(primeraLetra != primeraLetra.ToUpper()){
            return new ValidationResult("La primera letra debe ser mayuscula");
        }

        return ValidationResult.Success;
    }
}

}