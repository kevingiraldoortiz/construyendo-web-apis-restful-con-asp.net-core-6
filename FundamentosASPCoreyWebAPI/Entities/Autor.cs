﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using FundamentosASPCoreYWebAPI.Validaciones;

namespace FundamentosASPCoreYWebAPI.Entities
{
    public class Autor
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [StringLength(maximumLength: 120, ErrorMessage = "El campo {0} no debe de tener más de {1} carácteres")]
        public string Nombre { get; set; }
        public List<Libro> Libros { get; set; }

        public int Menor { get;set; }
        public int Mayor { get;set; }

        // esto se ejecuta despues de terminar las valiciones por atributo
        public IEnumerable<ValidationResult> Validate (ValidationContext context) {
            if(!string.IsNullOrEmpty(Nombre)){
                string primeraLetra = Nombre[0].ToString();

                if(primeraLetra != primeraLetra.ToUpper()){
                    yield return new ValidationResult("La primera letra debe ser mayuscula",
                        new string [] { nameof(Nombre) }
                    );
                }
            }

            if (Menor > Mayor){
                yield return new ValidationResult("Este valor no puede ser mas grande que el campo mayor");
            }
        }
        
    }
}